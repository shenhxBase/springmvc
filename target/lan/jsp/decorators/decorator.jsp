<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2014/9/9
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <sitemesh:write property='title'/>
    </title>

    <link href="/cj/css/bootstrap.min.css" rel="stylesheet">
   <%-- <link href="http://apps.bdimg.com/libs/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">--%>
    <script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.js"></script>
    <script src="http://apps.bdimg.com/libs/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <sitemesh:write property='head'/>

</head>
<body style="text-align: center">
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <form class="navbar-form navbar-right" role="form">
                    <div class="form-group">
                        <input type="text" placeholder="Email" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="password" placeholder="Password" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-success">Sign in</button>
                </form>
            </div>
            <!--/.navbar-collapse -->
        </div>
    </nav>
</header>
<hr/>
<sitemesh:write property='body'/>
<img src="img/b.jpg"/>
<hr/>
<footer>
    <%--<div class="footer" style="width: 100%;bottom: 0;background-color: #f2dede;height: 60px;position: absolute">
        <div class="container">
            <p class="text-muted">Test template by <a href="http://www.oschina.net">@spoom</a>.</p>
        </div>
    </div>--%>
    <div class="blog-footer"
         style="padding: 40px 0;color: #999;text-align: center;background-color: #f9f9f9;border-top: 1px solid #e5e5e5;">
        <p>Test template by <a href="http://www.oschina.net">@spoom</a>.</p>
        <p><a href="#">Back to top</a></p>
    </div>
</footer>
</body>
</html>
