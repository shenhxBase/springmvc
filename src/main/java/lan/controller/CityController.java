package lan.controller;

import lan.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Spoom on 2014/6/30.
 */
@Controller
public class CityController {

    @Autowired
    private CityService cityService;

    @RequestMapping("/city")
    public ModelAndView getCity(Integer id) throws Exception {
        try {
            String cityName = cityService.getCity(id).getCity();
            return new ModelAndView("jsp/success", "name", cityName);
        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("index");
        }
    }


    //验证码
    @RequestMapping("/code")
    public ModelAndView getCode(HttpServletRequest request) throws Exception {
        try {
            String str = (String) request.getSession().getAttribute("idCode");
            return new ModelAndView("jsp/code", "str", str);
        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("index");
        }
    }
}
