package lan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * Created by Spoom on 2014/7/2.
 */
@Controller
public class uploadController {


    @RequestMapping(value = "/upload",method = RequestMethod.POST)
    public String uploadFile(String name,@RequestParam("file")MultipartFile myfile,HttpServletRequest request){
        try {
            if(!myfile.isEmpty()){
                String path=request.getSession().getServletContext().getRealPath("/img");
                System.out.println("------");
                System.out.println(path);
                System.out.println(myfile.getOriginalFilename());
                System.out.println("------");
                myfile.transferTo(new File(path, myfile.getOriginalFilename()));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "jsp/upload_failure";
        }
        return "jsp/upload_success";
    }
}
