package lan.service;

import lan.model.City;

/**
 * Created by Spoom on 2014/6/30.
 */
public interface CityService {
    public City getCity(Integer id);
}
