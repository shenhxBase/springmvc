package lan.service;

import lan.dao.CityMapper;
import lan.model.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Spoom on 2014/6/30.
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityMapper cityMapper;
    @Override
    public City getCity(Integer id) {
        return cityMapper.selectByPrimaryKey(id);
    }
}
