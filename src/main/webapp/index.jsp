<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>My spring demo</title>
</head>
<body>
<div class="jumbotron">
    <div class="container">
        <h1>Spring MVC!</h1>

        <p>This is a template for a simple springmvc.</p>

        <form class="form-inline" action="city" name="getCity">
            <div class="form-group">
                <label for="exampleInputCity">City id </label>
                <input type="text" class="form-control" id="exampleInputCity" name="id" placeholder="city id">
            </div>
            <input type="submit" class="btn btn-success" value="submit">
        </form>
    </div>
</div>

<div class="jumbotron">
    <div class="container">
            <form class="form-inline" action="upload" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">###</div>
                        <input class="form-control" type="file" name="file" placeholder="Fiel name">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        <h2>jelly test</h2>

        <form action="code" method="post">
            <img id="code" onclick="this.src+=''" src="idCode"
                 style="cursor: pointer;" width="115" height="30" title="看不清？换一个">

                <input class="btn btn-info" type="submit" value="subm"/>
        </form>
    </div>
</div>

<img src="img/a.jpg">


</body>
</html>