import com.alibaba.fastjson.JSON;
import lan.model.City;
import lan.service.CityService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by Spoom on 2014/6/30.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:spring.xml","classpath*:spring-mybatis.xml"})
public class TestMybatis {


    private static final Logger logger=Logger.getLogger(TestMybatis.class);
    @Autowired
    private CityService cityService;

    @Test
    public void getUser(){
        City city=cityService.getCity(2);
        System.out.println("------");
        System.out.println(city.getCity());
        System.out.println("------");
    }

    @Test
    public void getCity(){
        City city=cityService.getCity(10);
        logger.info(JSON.toJSONString(city));
    }

}
